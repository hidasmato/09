const falseOrTrue = (as) => {
    let p = "" + as;
    if (typeof (as) === typeof (""))
        p = `"` + p + `"`;
    if (as)
        console.log(`${p} is true`);
    else {
        console.log(`${p} is false`);
    }
}
falseOrTrue(false);
falseOrTrue(true);
falseOrTrue(undefined);
falseOrTrue(0);
falseOrTrue(1);
falseOrTrue(null);
falseOrTrue(NaN);
falseOrTrue("");
falseOrTrue(" ");
falseOrTrue("string");
falseOrTrue({ a: 1 });

const trycatch = (num) => {
    try {
        switch (num) {
            case 1:
                throw 123;
                break;
            case 2:
                throw "wert"
                break;
            case 3:
                throw [1, 2, 3, 4, 5, 6]
                break;
            case 4:
                throw { message: "HowYouDear" }
                break;
            default:
                return 0;
                break;
        }
    } catch (error) {
        console.log("throw:", error)
        return -1;
    } finally {
        console.log(`Это было число ${num}`)
    }
}

for (let i = 0; i < 5; i++) {
    if (trycatch(i) == 0) console.log("Завершилось без ошибок");
}
